#!/bin/bash

# TODO: ajouter une commande grep permettant de chercher à l'intérieur de la note #
# TODO: revoir le système de passage d'argument aux fonctions

# Completion -------------------------------------------------------------------

declare -a COMMANDS=(new find image edit cat paste list reference graph delete)
declare -a COMMANDS_SHORT=(n f i e c p l r g d)

check_args() {
	for c in "${COMMANDS[@]}"; do
		[[ "${c}" == "${1}" ]] && return 0
	done
	for c in "${COMMANDS_SHORT[@]}"; do
		[[ "${c}" == "${1}" ]] && return 0
	done
	return 1
}

if [[ -n ${COMP_LINE} ]]; then
	for c in "${COMMANDS[@]}"; do
		[[ ${c:0:${#2}} == "${2,,}" ]] && echo "${c}"
	done
	exit
fi

if ! check_args "${1}"; then
	echo "Invalid argument"
	exit 1
fi

# Functions --------------------------------------------------------------------

# TODO: add grep to get matches inside notes

function find_matches() {
	matches=$(grep \
		--color=never \
		--no-filename \
		--recursive \
		--ignore-case "^#.[0-9].*${1}" "${zettelkasten}")
	[[ -n "${2}" ]] && matches=$(echo "${matches}" | grep --ignore-case "${2}")
	[[ -n "${3}" ]] && matches=$(echo "${matches}" | grep --ignore-case "${3}")
	[[ -n "${4}" ]] && matches=$(echo "${matches}" | grep --ignore-case "${4}")
	matches_formatted=$(echo "${matches}" | sed \
		--regexp-extended \
		--expression='s/# ([0-9]+) (.*)/\1 \2/')
	echo "${matches_formatted}"
}

function add_prefix_number() {
	# TODO: Compter le nombre de ligne en utilisant wc -l | wc -c #
	matches=$(find_matches "${1}" "${2}" "${3}" "${4}" "${5}")
	[[ -z "${matches}" ]] && exit 0
	output_with_number=$(echo "${matches}" |
		nl \
			--number-format=rn \
			--number-width=2 \
			--number-separator=" : " \
			--body-numbering=a)
	echo "${output_with_number}"
}

function format_as_markdown_ref() {
	md_ref=$(find_matches "${1}" "${2}" "${3}" "${4}" "${5}" |
		awk --field-separator ' : ' '{printf "[%s](%s.md)\n", $2 , $1}')
	echo "${md_ref}"
}

# Script -----------------------------------------------------------------------

date=$(date +%Y%m%d%H%M%S)
template="# ${date} :"
template_image="# ${date} :\n\n![](${date}.png)"

prev_dir=$(pwd)
cd "${zettelkasten}" || exit 1

case ${1} in
cat | c)
	cat "${2}.md"
	exit 0
	;;
delete | d)
	[[ -f ${2}.md ]] && rm "${2}.md"
	[[ -f ${2}.png ]] && rm "${2}.png"
	exit 0
	;;
edit | e)
	number_of_matches=$(wc -l </tmp/zettelkasten_buffer)
	if [[ -z ${2} ]] && ((number_of_matches > 0)); then
	    [[ ! -f /tmp/zettelkasten_buffer ]] && echo "no search yet" && exit 1
		file=$(head -n 1 </tmp/zettelkasten_buffer | awk '{print $3}')
		vi -c 'set spell spelllang=fr nowrap' "${file}.md"
		exit 0
	fi
	[[ -z ${2} ]] && echo 'missing second argument' && exit 1
	if ((${2} > 20230322103249)); then
		file=$(echo "${2}" | xargs | awk '{print $0}')
		vi -c 'set spell spelllang=fr nowrap' "${file}.md"
		exit 0
	fi
	[[ ! -f "/tmp/zettelkasten_buffer" ]] && echo "There any search yet" && exit 1
	file=$(grep "^\s*\<${2}\>" /tmp/zettelkasten_buffer | awk '{print $3}')
	vi -c 'set spell spelllang=fr nowrap' "${file}.md"
	exit 0
	;;
find | f)
	matches=$(add_prefix_number "${2}" "${3}" "${4}" "${5}")
	[[ -z "${matches}" ]] && exit 0
	matches_without_timestamp=$(echo "${matches}" | tee "/tmp/zettelkasten_buffer" | sed 's/: [0-9]*\s:\s/: /g')
	echo "${matches_without_timestamp}"
	exit 0
	;;
graph | g)
    standard_input=$(</dev/stdin)
    inkscape --export-filename="${zettelkasten}/${standard_input}.svg" --export-area-drawing --export-width=800 --export-height=600 >/dev/null 2>&1
    inkscape "${zettelkasten}/${standard_input}.svg" > /dev/null 2>&1
    echo "![${standard_input}](${standard_input}.svg)"
	exit 0
	;;
image | i)
	# TODO: with a file name vim is not open
	if [[ -z ${2} ]]; then
        flameshot gui -p "${date}.png" > /dev/null 2>&1
		echo -e "${template_image}" >"${date}.md"
		${TERMINAL} --hold vi "${date}.md" > /dev/null 2>&1

	else
		mv "$prev_dir/$2" "${zettelkasten}/${date}.png"
		echo -e "${template_image}" >"${date}.md"
		echo "${PS1}" >~/tmp/log
	fi
	exit 0
	;;
list | l) # usefull if I need more complex search (multiple pipe)
	files=$(find . -name "*.md" -exec head -n 1 {} \;)
	echo "${files}" | sort | sed 's/#\s//g'
	exit 0
	;;
new | n)
	echo -e "$template" >"${date}.md"
    ;;
reference | r)
    # TODO: use the standard output
	# [[ -z ${2} ]] && echo 'missing second argument' && exit 1
	matches=$(format_as_markdown_ref "${2}" "${3}" "${4}" "${5}")
	[[ -z "${matches}" ]] && exit 0
	echo "${matches}"
	exit 0
	;;
esac

vi -c 'set spell spelllang=fr nowrap' "${date}.md"

cd - || exit 0
