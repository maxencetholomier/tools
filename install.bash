cd /usr/local/bin || exit 1
links=$(find . -type l -exec readlink {} \; | grep "${tools}" | sed "s/.*\///g" )
for l in ${links}; do  sudo unlink "${l}" ; done
cd - || exit 1

sudo stow scripts -t /usr/local/bin
sudo stow aliases -t /usr/local/bin

