# Tools

This repository contains my scripts and snippets.

As I'm still a junior engineer I will probably modify a lot this repository.

## Installation

```bash
git clone https://gitlab.com:/maxencetholomier/tools tools
install.sh
```

## Credit 

Main of my knowledge here come from : 

* [Rob Muhlestein](https://github.com/rwxrob)
* [Luke Smith](https://lukesmith.xyz/)
* [Dereck Taylor](https://distro.tube/)
